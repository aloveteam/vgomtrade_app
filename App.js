import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { AsyncStorage } from 'react-native';
import store from './app/store';
import { AUTHENTICATED, UNAUTHENTICATED } from './app/actions/authentication';
import { FONTS_LOADED } from './app/actions/fonts';
import { Font } from 'expo';

import MainApp from './app/components/MainApp';

class App extends Component {
    componentWillMount() {
      AsyncStorage.removeItem('token');
    }

    async componentDidMount() {
        await Font.loadAsync({
            'Raleway-Black': require('./app/assets/fonts/Raleway/Raleway-Black.ttf'),
            'Raleway-Regular': require('./app/assets/fonts/Raleway/Raleway-Regular.ttf'),
            'Raleway-ExtraLightItalic': require('./app/assets/fonts/Raleway/Raleway-ExtraLightItalic.ttf'),
            'Lato-Light': require('./app/assets/fonts/Lato/Lato-Light.ttf'),
            'Lato-LightItalic': require('./app/assets/fonts/Lato/Lato-LightItalic.ttf'),
            'Lato-Black': require('./app/assets/fonts/Lato/Lato-Black.ttf'),
            'Lato-Hairline': require('./app/assets/fonts/Lato/Lato-Hairline.ttf'),
            'Lobster': require('./app/assets/fonts/Lobster/Lobster-Regular.ttf'),
        });

        store.dispatch({ type: FONTS_LOADED });
    }

  	render() {
    	return (
        <Provider store={store}>
          <MainApp />
        </Provider>
    	);
  	}
}

export default App;
