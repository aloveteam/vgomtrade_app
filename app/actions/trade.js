export const NEW_TRADE = 'NEW_TRADE';
export const NEW_TRADE_ERROR = 'NEW_TRADE_ERROR';
export const LOADING_TRADE = 'LOADING_TRADE';
export const REMOVE_TRADE = 'REMOVE_TRADE';
export const REMOVE_TRADE_ERROR = 'REMOVE_TRADE_ERROR';
import axios from 'axios';
import { AsyncStorage } from 'react-native';
import config from '../../configs/general';

export function initiateTrade(userItems, botItems) {
  return dispatch => {
	  AsyncStorage.getItem('token').then((token) => {
		if(token) {
		  	axios.post(config.API_SERVER + '/bot/trade', { items_to_send: botItems, items_to_receive: userItems}, { headers: {Authorization : 'Bearer ' + token}}).then((response) => {
				if(response.data.success){
					dispatch({
						type: NEW_TRADE,
						trade: response.data.trade
					});
				} else {
					dispatch({
						type: NEW_TRADE_ERROR,
						error: response.data.message
					});
				}
			});
		}
	 });
  }
}

export function loadingTrade() {
	return dispatch => {
		dispatch({
			type: LOADING_TRADE
		})
	}
}

export function removeTrade() {
  return dispatch => {
		dispatch({
			type: REMOVE_TRADE
		})
	}
}

export function removeTradeError() {
  return dispatch => {
		dispatch({
			type: REMOVE_TRADE_ERROR
		})
	}
}
