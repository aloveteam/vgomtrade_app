export const NEW_NOTIFICATION = 'NEW_NOTIFICATION';
export const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION';

export function newNotification(ntype, message) {
  return dispatch => {
    dispatch({
        type: NEW_NOTIFICATION,
        newNotification: {
          ntype: ntype,
          message: message,
          animation: 'slideInUp'
        }
    });
  }
}

export function deleteNotification(id) {
  return dispatch => {
    dispatch({
        type: DELETE_NOTIFICATION,
        id: id
    });
  }
}
