export const AUTHENTICATED = 'AUTHENTICATED';
export const UNAUTHENTICATED = 'UNAUTHENTICATED';
export const AUTHENTICATION_ERROR = 'AUTHENTICATION_ERROR';
export const RESET_APP_STATE = 'RESET_APP_STATE';
export const SET_USERS_ONLINE = 'SET_USERS_ONLINE';
export const SET_TRADES_MADE = 'SET_TRADES_MADE';
import { AsyncStorage } from 'react-native';
import { NEW_NOTIFICATION } from './notification';
import { OPEN, CLOSE } from './loginModal';

export function loginSuccessful(response) {
  return dispatch => {
    if(response.token) {
      setToken(response.token);
      dispatch({
          type: AUTHENTICATED,
          payload: response.user
      });

      dispatch({
          type: CLOSE
      });

      dispatch({
          type: NEW_NOTIFICATION,
          newNotification: {
            ntype: 'success',
            message: response.message
          }
      });
    }
  }
}

export function loginFailed(response) {
  return dispatch => {
    dispatch({
      type: AUTHENTICATION_ERROR,
      payload: 'Servers are to busy, please try again later.'
    });

    dispatch({
      type: NEW_NOTIFICATION,
      newNotification: {
        ntype: 'danger',
        message: 'Servers are to busy, please try again later.'
      }
    });
  }
}

export function userLogout() {
  return dispatch => {
      AsyncStorage.removeItem('token');
      dispatch({
        type: UNAUTHENTICATED
      });
      dispatch({
        type: RESET_APP_STATE
      });
  }
}

export function setUsersOnline(count) {
  return dispatch => {
    dispatch({
      type: SET_USERS_ONLINE,
      count: count
    })
  }
}

export function setTradesMade(count) {
  return dispatch => {
    dispatch({
      type: SET_TRADES_MADE,
      count: count
    })
  }
}

async function setToken(token) {
	await AsyncStorage.setItem('token', token);
}
