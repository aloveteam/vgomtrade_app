export const FETCH_USER_ITEMS = 'FETCH_USER_ITEMS';
export const FETCH_USER_ITEMS_ERROR = 'FETCH_USER_ITEMS_ERROR';
export const SELECT_USER_ITEMS_FOR_TRADE = 'SELECT_USER_ITEMS_FOR_TRADE';
export const REFRESH_AND_DESELECT = 'REFRESH_AND_DESELECT';
import { AsyncStorage } from 'react-native';
import config from '../../configs/general';
import axios from 'axios';

export function fetchItems() {
  return dispatch => {
	  AsyncStorage.getItem('token').then((token) => {
		if(token) {
		  axios.get(config.API_SERVER + '/user/inventory', { headers: {Authorization : 'Bearer ' + token}}).then((response) => {
				if(response.data.success){
					dispatch({
						type: FETCH_USER_ITEMS,
						items: response.data.items,
            message: response.data.message || ''
					});
				} else {
					dispatch({
						type: FETCH_USER_ITEMS_ERROR,
						message: response.data.message
					});
				}
			});
		}
	 });
  }
}

export function selectUserItemForTrade(id, price) {
  return dispatch => {
	  dispatch({
      type: SELECT_USER_ITEMS_FOR_TRADE,
      id: id,
      price: price
    });
	 }
}

export function refreshAndDeselect() {
	return dispatch => {
		dispatch({
			type: REFRESH_AND_DESELECT
		});
	}
}