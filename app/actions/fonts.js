export const FONTS_LOADED = 'FONTS_LOADED';

export function fontsLoaded() {
  return dispatch => {
    dispatch({
        type: FONTS_LOADED,
        loaded: true
    });
  }
}
