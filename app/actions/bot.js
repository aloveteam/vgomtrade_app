export const FETCH_BOT_ITEMS = 'FETCH_BOT_ITEMS';
export const FETCH_BOT_ITEMS_ERROR = 'FETCH_BOT_ITEMS_ERROR';
export const SELECT_BOT_ITEMS_FOR_TRADE = 'SELECT_BOT_ITEMS_FOR_TRADE';
export const FETCH_BOT_INFO = 'FETCH_BOT_INFO';
export const REFRESH_AND_DESELECT = 'REFRESH_AND_DESELECT';
import { AsyncStorage } from 'react-native';
import config from '../../configs/general';
import axios from 'axios';

export function fetchBotInventory() {
  return dispatch => {
	  AsyncStorage.getItem('token').then((token) => {
		if(token) {
		  axios.get(config.API_SERVER + '/bot/inventory', { headers: {Authorization : 'Bearer ' + token}}).then((response) => {
				if(response.data.success){
					dispatch({
						type: FETCH_BOT_ITEMS,
						items: response.data.items,
            message: response.data.message || ''
					});
				} else {
					dispatch({
						type: FETCH_BOT_ITEMS_ERROR,
						message: response.data.message
					});
				}
			});
		}
	 });
  }
}

export function getOurBotsInfo() {
  return dispatch => {
	  AsyncStorage.getItem('token').then((token) => {
		if(token) {
		  axios.get(config.API_SERVER + '/bot/me', { headers: {Authorization : 'Bearer ' + token}}).then((response) => {
				if(response.data.success){
					dispatch({
						type: FETCH_BOT_INFO,
						bot: response.data.bot
					});
				}
			});
		}
	 });
 }
}

export function selectBotItemForTrade(id, price) {
  return dispatch => {
	  dispatch({
		  type: SELECT_BOT_ITEMS_FOR_TRADE,
		  id: id,
		  price: price
		});
	 }
}

export function refreshAndDeselect() {
	return dispatch => {
		dispatch({
			type: REFRESH_AND_DESELECT
		});
	}
}