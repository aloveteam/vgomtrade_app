export const OPEN_ACCEPT = 'OPEN_ACCEPT';
export const CLOSE_ACCEPT = 'CLOSE_ACCEPT';

export function openAcceptModal() {
  return dispatch => {
    dispatch({
        type: OPEN_ACCEPT
    });
  }
}

export function closeAcceptModal() {
  return dispatch => {
      dispatch({
          type: CLOSE_ACCEPT
      });
  };
}
