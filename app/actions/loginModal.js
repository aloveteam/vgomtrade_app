export const OPEN = 'OPEN';
export const CLOSE = 'CLOSE';
import axios from 'axios';
import config from '../../configs/general';
import { AsyncStorage } from 'react-native';
import { AUTHENTICATED, AUTHENTICATION_ERROR, userAuthentication } from './authentication';
import { NEW_NOTIFICATION } from './notification';

export function openLoginModal() {
  return dispatch => {
    axios.get(config.API_SERVER + '/auth/opskins').then((response) => {
        dispatch({
            type: OPEN,
            opskinsURI: response.data.url
        });
    });

    dispatch({
        type: OPEN,
        opskinsURI: ''
    });
  }
}

export function closeLoginModal() {
  return dispatch => {
      dispatch({
          type: CLOSE
      });
  };
}
