import * as AuthenticationActions from './authentication';
import * as LoginModalActions from './loginModal';
import * as NotificationActions from './notification';
import * as FontsActions from './fonts';
import * as UserInventoryActions from './userInventory';
import * as BotActions from './bot';
import * as TradeActions from './trade';
import * as AcceptModalActions from './acceptModal';

export const ActionCreators = Object.assign({},
  AuthenticationActions,
  LoginModalActions,
  NotificationActions,
  FontsActions,
  UserInventoryActions,
  BotActions,
  TradeActions,
  AcceptModalActions
);
