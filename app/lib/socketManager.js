import io from 'socket.io-client';
import config from '../../configs/general';

const socketConnection = io(config.SOCKET_SERVER);

export default socketConnection;
