import { FONTS_LOADED } from '../actions/fonts';

const INITIAL_STATE = {
  loaded: false
}

export default function(state=INITIAL_STATE, action) {
  switch(action.type) {
    case FONTS_LOADED:
      return { ...state, loaded: true};
  }
  return state;
}
