import { RESET_APP_STATE } from '../actions/authentication';
import { NEW_TRADE, NEW_TRADE_ERROR, LOADING_TRADE, REMOVE_TRADE, REMOVE_TRADE_ERROR } from '../actions/trade';

const INITIAL_STATE = { trade: null, error: '', loading: false};

export default function(state=INITIAL_STATE, action) {
	switch(action.type) {
		case LOADING_TRADE:
			return { trade: null, error: '', loading: true }
		case NEW_TRADE:
			return { ...state, trade: action.trade, loading: false }
		case NEW_TRADE_ERROR:
			return { ...state, error: action.error, loading: false }
		case REMOVE_TRADE_ERROR:
			return { ...state, error: '', loading: false }
		case REMOVE_TRADE:
			return { ...state, trade: null, loading: false }
		case RESET_APP_STATE:
	    return INITIAL_STATE;
	}

	return state;
}
