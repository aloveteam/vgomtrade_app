import { FETCH_USER_ITEMS, FETCH_USER_ITEMS_ERROR, SELECT_USER_ITEMS_FOR_TRADE, REFRESH_AND_DESELECT } from '../actions/userInventory';
import { RESET_APP_STATE } from '../actions/authentication';

const INITIAL_STATE={items: [], itemsToTrade:[], totalPriceSelected: 0, message: '', refreshed: false};

function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}

export default function(state=INITIAL_STATE, action) {
  switch(action.type) {
    case FETCH_USER_ITEMS:
      return { ...state, items: action.items, itemsToTrade: [], totalPriceSelected: 0, message: action.message, refreshed: true };
    case FETCH_USER_ITEMS_ERROR:
      return { ...state, message: action.message, itemsToTrade: [], totalPriceSelected: 0 };
    case SELECT_USER_ITEMS_FOR_TRADE:
      const exist = state.itemsToTrade.indexOf(action.id);
      if(exist === -1) {
        return {
          ...state,
          itemsToTrade: [...state.itemsToTrade, action.id],
          totalPriceSelected: roundToTwo(state.totalPriceSelected += action.price)
        };
      } else {
        return {
          ...state,
          itemsToTrade: [
            ...state.itemsToTrade.slice(0, exist),
            ...state.itemsToTrade.slice(exist + 1)
          ],
          totalPriceSelected: roundToTwo(state.totalPriceSelected -= action.price)
        }
      }
	case REFRESH_AND_DESELECT:
		return { ...state, refreshed: false }
    case RESET_APP_STATE:
      return INITIAL_STATE;
  }
  return state;
}
