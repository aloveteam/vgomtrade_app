import { combineReducers } from 'redux';
import authentication from './authentication';
import loginModal from './loginModal';
import notification from './notification';
import fonts from './fonts';
import userInventory from './userInventory';
import bot from './bot';
import trade from './trade';
import acceptModal from './acceptModal';

export default combineReducers({
  authentication,
  loginModal,
  notification,
  fonts,
  userInventory,
  bot,
  trade,
  acceptModal
})
