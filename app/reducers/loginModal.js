import { OPEN, CLOSE } from '../actions/loginModal';
import { RESET_APP_STATE } from '../actions/authentication';

const INITIAL_STATE={ opened: false };

export default function(state=INITIAL_STATE, action) {
  switch(action.type) {
    case OPEN:
      return { ...state, opened: true, opskinsURI: action.opskinsURI};
    case CLOSE:
      return { ...state, opened: false };
    case RESET_APP_STATE:
      return INITIAL_STATE;
  }
  return state;
}
