import { OPEN_ACCEPT, CLOSE_ACCEPT } from '../actions/acceptModal';
import { RESET_APP_STATE } from '../actions/authentication';

const INITIAL_STATE={ opened: false };

export default function(state=INITIAL_STATE, action) {
  switch(action.type) {
    case OPEN_ACCEPT:
      return { ...state, opened: true };
    case CLOSE_ACCEPT:
      return { ...state, opened: false };
    case RESET_APP_STATE:
      return INITIAL_STATE;
  }
  return state;
}
