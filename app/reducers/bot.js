import { FETCH_BOT_ITEMS, FETCH_BOT_ITEMS_ERROR, SELECT_BOT_ITEMS_FOR_TRADE, FETCH_BOT_INFO, REFRESH_AND_DESELECT } from '../actions/bot';
import { RESET_APP_STATE } from '../actions/authentication';

const INITIAL_STATE = {info: {}, items: [], itemsToTrade:[], totalPriceSelected: 0, message: '', refreshed: false};

function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}

export default function(state=INITIAL_STATE, action) {
  switch(action.type) {
    case FETCH_BOT_ITEMS:
      return { ...state, items: action.items, itemsToTrade: [], totalPriceSelected: 0, message: action.message, refreshed: true };
    case FETCH_BOT_ITEMS_ERROR:
      return { ...state, message: action.message, itemsToTrade: [], totalPriceSelected: 0 };
    case SELECT_BOT_ITEMS_FOR_TRADE:
      const exist = state.itemsToTrade.indexOf(action.id);
      if(exist === -1) {
        return {
          ...state,
          itemsToTrade: [...state.itemsToTrade, action.id],
          totalPriceSelected: roundToTwo(state.totalPriceSelected += action.price)
        };
      } else {
        return {
          ...state,
          itemsToTrade: [
            ...state.itemsToTrade.slice(0, exist),
            ...state.itemsToTrade.slice(exist + 1)
          ],
          totalPriceSelected: roundToTwo(state.totalPriceSelected -= action.price)
        }
      }
    case FETCH_BOT_INFO:
      return { ...state, info: action.bot };
	case REFRESH_AND_DESELECT:
		return { ...state, refreshed: false }
    case RESET_APP_STATE:
      return INITIAL_STATE;
  }
  return state;
}
