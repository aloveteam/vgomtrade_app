import { NEW_NOTIFICATION, DELETE_NOTIFICATION, ANIMATE_DELETE_NOTIFICATION } from '../actions/notification';

const INITIAL_STATE=[];
let notificationID = 0;

export default function(state=INITIAL_STATE, action) {
  switch(action.type) {
    case NEW_NOTIFICATION:
      notificationID++;
      action.newNotification.id = notificationID;
      return [ ...state, action.newNotification ];
    case DELETE_NOTIFICATION:
      let indexToDelete = state.findIndex(notification => notification.id === action.id);
      return [
          ...state.slice(0, indexToDelete),
          ...state.slice(indexToDelete + 1)
        ];
    }
  return state;
}
