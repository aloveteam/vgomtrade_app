import { AUTHENTICATED, UNAUTHENTICATED, AUTHENTICATION_ERROR, SET_USERS_ONLINE, SET_TRADES_MADE } from '../actions/authentication';
import { RESET_APP_STATE } from '../actions/authentication';

const INITIAL_STATE = { authenticated: false, user: null, error: '', usersOnline: 0, tradesMade: 0 };

export default function(state=INITIAL_STATE, action) {
  switch(action.type) {
    case AUTHENTICATED:
      return { ...state, authenticated: true, user: action.payload };
    case UNAUTHENTICATED:
      return { ...state, authenticated: false };
    case AUTHENTICATION_ERROR:
      return { ...state, error: action.payload };
    case SET_USERS_ONLINE:
      return { ...state, usersOnline: action.count };
    case SET_TRADES_MADE:
      return { ...state, tradesMade: state.tradesMade += action.count }
    case RESET_APP_STATE:
      return INITIAL_STATE;
  }
  return state;
}
