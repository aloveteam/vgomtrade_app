import React, { Component } from 'react';
import { View, Text, WebView, Image, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './NotificationCSS';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';
import { deleteNotification } from '../actions/notification';

const notificationColors = {
  success: ['#44a544', '#52b952'],
  info: ['#138dff', '#19a0ff'],
  warning: ['#e8a619', '#ddbd1e'],
  danger: ['#b63423', '#dd351e']
}

class Notification extends Component {

  removeNotification(id) {
    this.props.deleteNotification(id);
  }

  listAllNotifications() {
    return this.props.notifications.map((notification, index) => (
      <Animatable.View key={notification.id} animation="slideInUp">
        <TouchableOpacity onPress={() => this.removeNotification(notification.id)}>
            <LinearGradient style={[styles.notificationFrame]} colors={notificationColors[notification.ntype]} start={{x: 0.0, y: 1.0}} end={{x:1.0, y: 1.0}}>
              <Icon name="rocket" size={20} color="#fff" style={[styles.notificationIcon]} />
              <Text style={[styles.notificationText]}>{notification.message}</Text>
            </LinearGradient>
        </TouchableOpacity>
      </Animatable.View>
    ))
  }

  render() {
    return (
      <View style={[styles.notificationArea]}>
        { this.listAllNotifications() }
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.authentication.error,
    authenticated: state.authentication.authenticated,
    notifications: state.notification
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    deleteNotification: deleteNotification
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Notification);
