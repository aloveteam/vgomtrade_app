import React, { Component } from 'react';
import styles from './UserSideMenuCSS';
import { NavigationActions, DrawerItems } from 'react-navigation';
import { ScrollView, Text, View, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { LinearGradient } from 'expo';
import { userLogout } from '../actions/authentication';
import { newNotification } from '../actions/notification';

class UserSideMenu extends Component {

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  logoutUser = () => {
    this.props.userLogout();
    this.props.navigation.navigate('Auth');
    this.props.newNotification('success', 'You have been successfully logged out.');
  }

  render() {
    return (
        <SafeAreaView style={styles.container}>
		  <LinearGradient
    		style={styles.header}
    		colors={['#ff9b2c', '#ff3187']}
    		start={{x: 0.0, y: 1.0}} end={{x:1.0, y: 0.0}}
    		>
			<Image source={{ uri: this.props.authenticatedUser.avatar }} style={styles.image} />
			<Text style={styles.username}>{ this.props.authenticatedUser.username }</Text>
		  </LinearGradient>
          <ScrollView>
            <View style={{ flex: 1 }}>
                <Text style={styles.sectionHeadingStyle}>
                  MENU
                </Text>
              <View style={styles.navSectionStyle}>
                <TouchableOpacity onPress={this.navigateToScreen('Trade')}>
                  <Text style={styles.navItemStyle}>Trade</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.logoutUser}>
                  <Text style={styles.navItemStyle}>Logout</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
    )
  }
};

function mapStateToProps(state) {
  return {
    fontsLoaded: state.fonts.loaded,
    authenticatedUser: state.authentication.user
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    userLogout: userLogout,
    newNotification: newNotification
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(UserSideMenu);
