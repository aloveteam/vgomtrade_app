import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { LinearGradient } from 'expo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './InventoryListCSS';
import { fetchItems, selectUserItemForTrade } from '../actions/userInventory';

const numColumns = 3;

const itemColors = {
	stroke: ['rgba(77,72,99,0.55)', 'rgba(77,72,99,0.0)'],
  strokeSelected: ['#ff9b2c', '#ff3187'],
	inner: ['#300a48', 'rgba(40,5,62,1)']
}

class InventoryItem extends Component {
  _onPress = () => {
    this.props.onPressItem(this.props.id);
    this.props.selectUserItemForTrade(this.props.id, this.props.price);
  };

  render() {
    const backgroundColor = this.props.selected ? '#ff9b2c' : 'transparent';
    const borderSize = this.props.selected ? { padding: 2 } : {};
    const hover = this.props.selected ? { opacity: 1 } : { opacity: 0 };
    return (
      <TouchableOpacity
        id={this.props.item.id}
        style={[styles.item, { backgroundColor: backgroundColor, opacity: this.props.item.available ? 1 : 0.5 }]}
		    onPress={this._onPress}
				disabled={!this.props.item.available}
      >
    		<LinearGradient colors={this.props.selected ? itemColors.strokeSelected : itemColors.stroke} start={{x: 0.0, y: 0.0}} end={{x:0.0, y: 1.0}} locations={[0, 1]} style={[styles.stroke, borderSize]}>
    			<LinearGradient colors={itemColors.inner} start={{x: 1.0, y: 0.0}} end={{x:1.0, y: 1.0}} locations={[0, 1]} style={styles.inner}>
            		<Image source={{ uri: this.props.item.image['300px'] }} style={styles.image}/>
    				<LinearGradient colors={['#ff3187', '#ff9b2c']} start={{x: 1.0, y: 0.0}} end={{x:0.0, y: 1.0}} locations={[0, 1]} style={[styles.price]}>
    				{ this.props.fontsLoaded ? (<Text style={styles.priceText}>{ this.props.item.suggested_price }</Text>) : null }
    				</LinearGradient>
            <View style={[styles.hover, hover]}>
              <View style={[styles.skinColor, { backgroundColor: this.props.item.color}]}></View>
              { this.props.fontsLoaded ? (<Text style={styles.itemName}>{ this.props.item.name }</Text>) : null }
            </View>
    			</LinearGradient>
    		</LinearGradient>
      </TouchableOpacity>
    );
  }
}

function mapStateToProps(state) {
  return {
	  fontsLoaded: state.fonts.loaded
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    selectUserItemForTrade: selectUserItemForTrade
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(InventoryItem);
