import {
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  notificationArea: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0
  },
  notificationFrame: {
    padding: 15,
    marginRight: 10,
    marginLeft: 10,
    marginBottom: 10,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  notificationText: {
    color: '#fff',
    flex: 1,
    flexWrap: 'wrap'
  },
  notificationIcon: {
    marginRight: 15
  }
});

export default styles;
