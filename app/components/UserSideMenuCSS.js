import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#240735'
  },
  header: {
    height: 200,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    height: 100,
    width: 100,
    resizeMode: 'contain',
    borderRadius: 50,
    padding: 6
  },
  navItemStyle: {
    padding: 15,
    color: '#fff'
  },
  navSectionStyle: {
    backgroundColor: 'rgba(255,255,255,0.05)'
  },
  sectionHeadingStyle: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    backgroundColor: 'rgba(255,255,255,0.1)',
    color: '#fff',
    letterSpacing: 5,
    textAlign: 'center'
  },
  username: {
    color: '#fff',
    fontFamily: 'Lato-Black',
    marginTop: 10
  }
});

export default styles;
