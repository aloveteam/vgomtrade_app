import React from 'react';
import { Text, View, FlatList, Dimensions, ActivityIndicator, StatusBar, Image, TouchableOpacity } from 'react-native';
import styles from './InventoryListCSS';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchBotInventory, refreshAndDeselect } from '../actions/bot';
import { LinearGradient } from 'expo';
import BotInventoryItem from './BotInventoryItem';

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

const numColumns = 3;

class BotInventoryList extends React.Component {
  state = {selected: (new Map(): Map<string, boolean>)};

  _keyExtractor = (item, index) => item.id;

  _onPressItem = (id: string, price: number) => {
     this.setState((state) => {
       const selected = new Map(state.selected);
       selected.set(id, !selected.get(id));
       return {selected};
     });
  };

  componentDidMount() {
	  this.props.fetchBotInventory();
  }

    componentWillReceiveProps(nextProps){
	  if(nextProps.refreshed !== this.props.refreshed) {
		  if(nextProps.refreshed) {
			  this.setState({
				  selected: (new Map(): Map<string, boolean>)
			  })
		  }
	  }

	  this.props.refreshAndDeselect();
  }

  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <BotInventoryItem
        id={item.id}
        price={item.suggested_price}
        item={item}
        onPressItem={this._onPressItem}
        selected={!!this.state.selected.get(item.id)}
      />
    );
  };

  render() {
    return (
		<View style={{ flex: 1 }}>
		{
			this.props.errorMessage.length === 0 ?
			(
				this.props.botInventory.length > 0 ?
				(
				  <FlatList
  					data={formatData(this.props.botInventory, numColumns)}
  					style={styles.container}
  					renderItem={this.renderItem}
  					numColumns={numColumns}
            extraData={this.state}
				  />
				) :
				(
					<View style={styles.loaderContainer}>
						<ActivityIndicator />
						<StatusBar barStyle="default" />
						<Text style={{ color: '#fff' }}>Loading bots inventory...</Text>
					</View>
				)
	  		) :
			(
				<View style={styles.loaderContainer}>
					<Text style={{ color: '#fff', textAlign: 'center' }}>{this.props.errorMessage}</Text>
				</View>
			)
		}
		</View>
    );
  }
}

function mapStateToProps(state) {
  return {
	  botInventory: state.bot.items,
	  fontsLoaded: state.fonts.loaded,
	  errorMessage: state.bot.message,
	  refreshed: state.bot.refreshed
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
	  fetchBotInventory: fetchBotInventory,
	  refreshAndDeselect: refreshAndDeselect
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(BotInventoryList);
