import React, { Component } from 'react';
import { ScrollView, View, Easing, Animated } from 'react-native';
import LoginScreen from './screens/LoginScreen';
import TradeScreen from './screens/TradeScreen';
import { LinearGradient, Font } from 'expo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createDrawerNavigator, createSwitchNavigator, DrawerItems, SafeAreaView } from 'react-navigation';
import { fontsLoaded } from '../actions/fonts';
import Notification from './Notification';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import UserSideMenu from './UserSideMenu';
import socket from '../lib/socketManager';
import { setUsersOnline, loginSuccessful, loginFailed, setTradesMade } from '../actions/authentication';

class MainApp extends Component {
  componentWillMount() {
    socket.on('user count', count => {
      this.props.setUsersOnline(count);
    });

    socket.on('trades count', count => {
      this.props.setTradesMade(count.trades);
    });

    socket.on('login successful', (response) => {
      this.props.loginSuccessful(response);
    });

    socket.on('login failed', (response) => {
      this.props.loginFailed(response);
    });
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <SwitchNavigation />
        <Notification />
      </View>
    )
  }
}

const AppStackNavigator = createDrawerNavigator({
    Trade: {
      screen: TradeScreen
    }
  },
  {
    contentComponent: UserSideMenu,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
  }
);

const AuthStackNavigator = createDrawerNavigator({
  Login: {
    screen: LoginScreen
  }
});

const SwitchNavigation = createSwitchNavigator({
    AuthLoading: AuthLoadingScreen,
    App: AppStackNavigator,
    Auth: AuthStackNavigator,
  },
  {
    initialRouteName: 'AuthLoading',
});

function mapStateToProps(state) {
  return {}
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    fontsLoaded: fontsLoaded,
    setUsersOnline: setUsersOnline,
    loginSuccessful: loginSuccessful,
    loginFailed: loginFailed,
    setTradesMade: setTradesMade
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(MainApp);
