import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ActivityIndicator, StatusBar } from 'react-native';
import { LinearGradient } from 'expo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './InitiateTradeCSS';
import { initiateTrade, loadingTrade, removeTrade, removeTradeError } from '../../actions/trade';
import { openAcceptModal } from '../../actions/acceptModal';

class InitiateTrade extends Component {

  isAcceptable = (userPrice, botPrice) => {
    if(userPrice == 0 && botPrice == 0) {
      return false;
    }

    if(userPrice >= botPrice) {
      return true;
    }

    return false;
  }

  openTrade = () => {
    this.props.openAcceptModal();
  }

  initiateTrade = () => {
    this.props.loadingTrade();
	  this.props.initiateTrade(this.props.userItemsForTrade, this.props.botItemsForTrade);
  }

  goBackError = () => {
    this.props.removeTradeError();
  }

  goBack = () => {
    this.props.removeTrade();
  }

  render() {
    return (
      !this.props.tradeIsLoading ?
        (
          !this.props.trade && this.props.tradeError.length === 0 ? (
            <View style={styles.container}>
              { this.props.fontsLoaded ? (<Text style={styles.tradeText}>Hello {this.props.authenticatedUser.username}, have you selected items that you want get rid off and chose something nice from our bot?</Text>) : null }
              { this.props.fontsLoaded ? (<Text style={styles.tradeTextNo}>If you did, you can click button below to initiate a trade.</Text>) : null }
              <View style={styles.tradeDeal}>
                <View style={styles.tradeUserDeal}>
                  <Image source={{ uri: this.props.authenticatedUser.avatar }} style={styles.tradeUserDealImage}/>
                  <Text style={styles.tradeUserText}>You give { this.props.userItemsSelected } skins, valued at { this.props.userItemsPriceSelected }.</Text>
                </View>
                <View style={[styles.tradeBotDeal, { marginBottom: 5 }]}>
                  <Image source={ require('../../assets/bot_logo.png') } style={styles.tradeUserDealImage}/>
                  <Text style={styles.tradeUserText}>You get { this.props.botItemsSelected } skins, valued at { this.props.botItemsPriceSelected }.</Text>
                </View>
                <View style={[styles.acceptableFrame, { backgroundColor: this.isAcceptable(this.props.userItemsPriceSelected, this.props.botItemsPriceSelected) ? 'green' : 'red' }]}>
                  <Text style={styles.acceptableText}>{ this.isAcceptable(this.props.userItemsPriceSelected, this.props.botItemsPriceSelected) ? 'Acceptable' : 'Unacceptable' }</Text>
                </View>
              </View>
              <View style={{ flex: 1, justifyContent: 'flex-end', opacity: this.isAcceptable(this.props.userItemsPriceSelected, this.props.botItemsPriceSelected) ? 1 : 0.1}}>
                <LinearGradient
                  style={styles.buttonGradient}
                  colors={['#ff9b2c', '#ff3187']}
                  start={{x: 0.0, y: 1.0}} end={{x:1.0, y: 0.0}}
                >
                  <TouchableOpacity
                    onPress={this.initiateTrade}
                    style={styles.buttonContainer}
                    disabled={!this.isAcceptable(this.props.userItemsPriceSelected, this.props.botItemsPriceSelected)}
                  >
                    {
                      this.props.fontsLoaded ?
                      (
                        <View style={styles.buttonTextFrame}>
                          <Text style={styles.buttonText}>TRADE</Text>
                          <Text style={styles.buttonTextSmall}>NOW</Text>
                        </View>
                      ) : null
                    }
                  </TouchableOpacity>
                </LinearGradient>
              </View>
            </View>
          ) : (
            this.props.tradeError.length > 0 ?
            (
              <View style={styles.container}>
                { this.props.fontsLoaded ? (<Text style={styles.tradeTextHeader}>Opssss</Text> ) : null }
                { this.props.fontsLoaded ? (<Text style={styles.tradeTextNo}>{ this.props.tradeError }.</Text> ) : null }
                <View style={{ flex: -1, marginTop: 10 }}>
                  <TouchableOpacity
                    onPress={this.goBackError}
                    style={styles.buttonContainer}
                  >
                    {
                      this.props.fontsLoaded ?
                      (
                        <View style={styles.buttonTextFrame2}>
                          <Text style={styles.buttonTextSmall}>GO BACK</Text>
                        </View>
                      ) : null
                    }
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View style={styles.container}>
                { this.props.fontsLoaded ? (<Text style={styles.tradeTextHeader}>Congratulations</Text> ) : null }
                { this.props.fontsLoaded ? (<Text style={styles.tradeTextNo}>Our bot has successfully sent a trade to you with code {this.props.trade.message_code}, you have 15 minutes to accept it before our bot cancel it.</Text> ) : null }
                { this.props.fontsLoaded ? (<Text style={styles.tradeText}>You can accept it by clicking on button below.</Text> ) : null }
                <View style={{ flex: 1, marginTop: 20 }}>
                  <LinearGradient
                    style={styles.buttonGradient}
                    colors={['#ff9b2c', '#ff3187']}
                    start={{x: 0.0, y: 1.0}} end={{x:1.0, y: 0.0}}
                  >
                    <TouchableOpacity
                      onPress={this.openTrade}
                      style={styles.buttonContainer}
                    >
                      {
                        this.props.fontsLoaded ?
                        (
                          <View style={styles.buttonTextFrame}>
                            <Text style={styles.buttonText}>ACCEPT</Text>
                            <Text style={styles.buttonTextSmall}>OFFER</Text>
                          </View>
                        ) : null
                      }
                    </TouchableOpacity>
                  </LinearGradient>
                </View>
                <View style={{ flex: -1, marginTop: 10 }}>
                  <TouchableOpacity
                    onPress={this.goBack}
                    style={styles.buttonContainer}
                  >
                    {
                      this.props.fontsLoaded ?
                      (
                        <View style={styles.buttonTextFrame2}>
                          <Text style={styles.buttonTextSmall}>GO BACK</Text>
                        </View>
                      ) : null
                    }
                  </TouchableOpacity>
                </View>
              </View>
            )
          )
        ) : (
          <View style={styles.container}>
            <ActivityIndicator />
						<StatusBar barStyle="default" />
						<Text style={{ color: '#fff' }}>Processing your trade offer...</Text>
          </View>
        )

    )
  }
}


function mapStateToProps(state) {
  return {
    fontsLoaded: state.fonts.loaded,
    authenticatedUser: state.authentication.user,
    userItemsSelected: state.userInventory.itemsToTrade.length,
    userItemsPriceSelected: state.userInventory.totalPriceSelected,
    botItemsSelected: state.bot.itemsToTrade.length,
    botItemsPriceSelected: state.bot.totalPriceSelected,
    ourBotsInfo: state.bot.info,
    userItemsForTrade: state.userInventory.itemsToTrade,
    botItemsForTrade: state.bot.itemsToTrade,
    tradeIsLoading: state.trade.loading,
    trade: state.trade.trade,
    tradeError: state.trade.error
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    initiateTrade: initiateTrade,
    loadingTrade: loadingTrade,
    removeTrade: removeTrade,
    removeTradeError: removeTradeError,
    openAcceptModal: openAcceptModal
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(InitiateTrade);
