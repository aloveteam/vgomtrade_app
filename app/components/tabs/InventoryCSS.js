import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    slide: {
      flex: -1
    },
    frame: {
      paddingVertical: 25,
      paddingHorizontal: 15,
      borderRadius: 7,
      alignItems: 'center',
      width: '80%',
      alignSelf: 'center'
    },
    frameFirst: {
      marginLeft: 0
    },
    frameLast: {
      marginRight: 0
    },
    username: {
      color: '#fff',
      textAlign: 'center',
      fontFamily: 'Lato-LightItalic'
    },
    textBig: {
      color: '#fff',
      textAlign: 'center',
      fontFamily: 'Lato-Black',
      fontSize: 30,
      letterSpacing: 5
    },
    textSmall: {
      color: '#fff',
      textAlign: 'center',
      fontFamily: 'Lato-Hairline',
      letterSpacing: 4
    },
    hr: {
      width: '15%',
      height: 3,
      marginVertical: 10,
      backgroundColor: '#fff'
    },
    image: {
      position: 'absolute',
      top: '30%',
      width: 60,
      height: 60,
      resizeMode: 'contain',
	  borderRadius: 60
    },
    imageRight: {
      right: 0
    },
    imageLeft: {
      left: 0
    },
	inventoryWrapper: {
		flex: 1,
    marginTop: 20,
    backgroundColor: 'rgba(255,255,255,0.02)',
    padding: 10,
    borderRadius: 3
	},
    inventory: {
      flex: 1,
    },
	inventorFilters: {
		flexDirection: 'row',
		flexWrap: 'nowrap',
		alignItems: 'flex-start',
		justifyContent: 'flex-start'
	},
    inventorySort: {
      flex: 3,
      borderRadius: 50
    },
    sortPicker: {
      height: 50,
      width: 100,
      backgroundColor: '#4a2461',
      color: '#fff'
    },
	inventorySearch: {
      flex: 0,
      borderRadius: 50,
		backgroundColor: 'blue'
    },
	searchInput: {
      height: 50,
      width: 100,
      backgroundColor: '#4a2461',
      color: '#fff',
	  borderWidth: 0
    },
    inventorySelected: {
      flex: -1,
      flexDirection: 'row',
      marginBottom: 10
    },
    inventorySelectedColumn: {
      flex: 1,
      marginHorizontal: 5,
      backgroundColor: 'rgba(255,255,255,0.1)',
      borderRadius: 30,
      padding: 7,
      justifyContent: 'center',
      alignItems: 'center'
    },
    inventorySelectedText: {
      color: '#fff',
      fontFamily: 'Lato-Light'
    },
    inventorySelectedColumnIcon: {
      flex: -1,
      flexGrow: 0
    }
});

export default styles;
