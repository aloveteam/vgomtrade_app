import { StyleSheet, Dimensions } from 'react-native';

let width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      padding: 20
    },
    tradeText: {
      color: '#fff',
      textAlign: 'center',
      fontFamily: 'Raleway-Black',
      marginBottom: 15
    },
    tradeTextHeader: {
      color: '#fff',
      textAlign: 'center',
      fontFamily: 'Lobster',
      marginBottom: 20,
      fontSize: 30
    },
    tradeTextYes: {
      fontFamily: 'Lato-Light',
      color: '#fff',
      textAlign: 'center',
      marginBottom: 15
    },
    tradeTextNo: {
      fontFamily: 'Lato-LightItalic',
      color: '#fff',
      textAlign: 'center',
      marginBottom: 15
    },
    buttonGradient: {
  		borderRadius: 50,
  		height: 100,
  		width: '80%',
  		alignItems: 'center',
  		justifyContent: 'center'
  	},
  	buttonContainer: {
  		width: 1 * (width - 100),
  		height: 92,
  		alignItems: 'center',
  		borderRadius: 50
  	},
  	buttonTextFrame: {
  		width: 1 * (width - 108),
  		height: '100%',
  		backgroundColor: '#230437',
  		borderRadius: 50,
  	},
    buttonTextFrame2: {
  		backgroundColor: '#230437',
  		borderRadius: 50,
      padding: 15
  	},
  	buttonText: {
  		textAlign: 'center',
  		color: '#ff7949',
  		marginTop: 15,
  		fontSize: 30,
  		fontFamily: 'Raleway-Black'
  	},
  	buttonTextSmall: {
  		textAlign: 'center',
  		color: '#fff',
  		fontSize: 17,
  		fontFamily: 'Raleway-Regular'
  	},
    tradeDeal: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%'
    },
    tradeUserDeal: {
      padding: 5,
      borderRadius: 100,
      backgroundColor: 'rgba(255,255,255,0.1)',
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 15
    },
    tradeBotDeal: {
      padding: 5,
      borderRadius: 100,
      backgroundColor: 'rgba(255,255,255,0.1)',
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 15
    },
    tradeUserDealImage: {
      flex: -1,
      width: 50,
      height: 50,
      resizeMode: 'contain',
      borderRadius: 50,
      marginHorizontal: 5
    },
    tradeUserText: {
      color: '#fff',
      fontFamily: 'Lato-Black'
    },
    acceptableFrame: {
      paddingHorizontal: 10,
      paddingVertical: 5,
      borderRadius: 100,
      backgroundColor: 'green'
    },
    acceptableText: {
      color: '#fff',
      fontFamily: 'Lato-Black'
    }
});

export default styles;
