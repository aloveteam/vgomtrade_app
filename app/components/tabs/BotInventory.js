import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo';
import styles from './InventoryCSS';
import BotInventoryList from '../BotInventoryList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/Feather';
import { fetchBotInventory, getOurBotsInfo } from '../../actions/bot';

class BotInventory extends Component {

  componentDidMount() {
    this.props.getOurBotsInfo();
  }

  refreshBotInventory = () => {
    this.props.fetchBotInventory();
  }

  render() {
    return (
      <View style={{ flex: 1, width: '100%' }}>
        <View style={styles.slide}>
          <LinearGradient colors={['#138dff', '#cc19ff']} start={{x: 1.0, y: 0.0}} end={{x:0.0, y: 1.0}} locations={[0, 1]} style={[styles.frame]}>
            { this.props.fontsLoaded ? (<Text style={styles.username}>{ this.props.ourBotsInfo.name }</Text>) : null }
            <View style={styles.hr}></View>
            { this.props.fontsLoaded ? (<Text style={styles.textBig}>OUR</Text>) : null }
            { this.props.fontsLoaded ? (<Text style={styles.textSmall}>INVENTORY</Text>) : null }
          </LinearGradient>
          <Image source={ require('../../assets/bot_logo.png') } style={[styles.image, styles.imageRight]}/>
        </View>
        <View style={styles.inventoryWrapper}>
          <View style={{ flex: 1 }}>
              <View style={styles.inventorySelected}>
                 <View style={[styles.inventorySelectedColumn]}>
                   { this.props.fontsLoaded ? (<Text style={[styles.inventorySelectedText]}>Selected - { this.props.botItemsSelected }</Text>) : null }
                 </View>
                 <View style={[styles.inventorySelectedColumn]}>
                   { this.props.fontsLoaded ? (<Text style={[styles.inventorySelectedText]}>Price - { this.props.botItemsPriceSelected }</Text>) : null }
                 </View>
                 <TouchableOpacity style={[styles.inventorySelectedColumn, styles.inventorySelectedColumnIcon, { opacity: this.props.isRefreshed ? 0 : 1 }]} disabled={this.props.isRefreshed} onPress={this.refreshBotInventory}>
                   <Icon name="refresh-ccw" size={20} color="#fff" style={[styles.notificationIcon]} />
                 </TouchableOpacity>
              </View>
              <ScrollView style={styles.inventory}>
                <BotInventoryList />
              </ScrollView>
          </View>
        </View>
      </View>
    )
  }
}


function mapStateToProps(state) {
  return {
    fontsLoaded: state.fonts.loaded,
	  authenticatedUser: state.authentication.user,
    botItemsSelected: state.bot.itemsToTrade.length,
    botItemsPriceSelected: state.bot.totalPriceSelected,
    ourBotsInfo: state.bot.info,
    isRefreshed: state.bot.refreshed
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchBotInventory: fetchBotInventory,
    getOurBotsInfo: getOurBotsInfo
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(BotInventory);
