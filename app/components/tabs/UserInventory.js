import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo';
import styles from './InventoryCSS';
import InventoryList from '../InventoryList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/Feather';
import { fetchItems } from '../../actions/userInventory';

class UserInventory extends Component {
  refreshUserInventory = () => {
    this.props.fetchUserInventroy();
  }

  render() {
    return (
      <View style={{ flex: 1, width: '100%' }}>
        <View style={styles.slide}>
          <LinearGradient colors={['#ff3187', '#ff9b2c']} start={{x: 1.0, y: 0.0}} end={{x:0.0, y: 1.0}} locations={[0, 1]} style={[styles.frame, styles.frameFirst ]}>
            { this.props.fontsLoaded ? (<Text style={styles.username}>{ this.props.authenticatedUser.username }</Text>) : null }
            <View style={styles.hr}></View>
            { this.props.fontsLoaded ? (<Text style={styles.textBig}>YOUR</Text>) : null }
            { this.props.fontsLoaded ? (<Text style={styles.textSmall}>INVENTORY</Text>) : null }
          </LinearGradient>
          <Image source={{ uri: this.props.authenticatedUser.avatar }} style={[styles.image, styles.imageLeft]}/>
        </View>
        <View style={styles.inventoryWrapper}>
            <View style={{ flex: 1 }}>
              <View style={styles.inventorySelected}>
                 <View style={[styles.inventorySelectedColumn]}>
                   { this.props.fontsLoaded ? (<Text style={[styles.inventorySelectedText]}>Selected - { this.props.userItemsSelected }</Text>) : null }
                 </View>
                 <View style={[styles.inventorySelectedColumn]}>
                   { this.props.fontsLoaded ? (<Text style={[styles.inventorySelectedText]}>Price - { this.props.userItemsPriceSelected }</Text>) : null }
                 </View>
                 <TouchableOpacity style={[styles.inventorySelectedColumn, styles.inventorySelectedColumnIcon, { opacity: this.props.isRefreshed ? 0 : 1 }]} disabled={this.props.isRefreshed} onPress={this.refreshUserInventory}>
                   <Icon name="refresh-ccw" size={20} color="#fff" style={[styles.notificationIcon]} />
                 </TouchableOpacity>
              </View>
              <ScrollView style={styles.inventory}>
                 <InventoryList />
              </ScrollView>
          </View>
        </View>
      </View>
    )
  }
}


function mapStateToProps(state) {
  return {
    fontsLoaded: state.fonts.loaded,
	  authenticatedUser: state.authentication.user,
    userItemsSelected: state.userInventory.itemsToTrade.length,
    userItemsPriceSelected: state.userInventory.totalPriceSelected,
    isRefreshed: state.userInventory.refreshed
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchUserInventroy: fetchItems
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(UserInventory);
