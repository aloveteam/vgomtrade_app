import {
  StyleSheet,
  Dimensions
} from 'react-native';

let width = Dimensions.get('window').width;

const styles = StyleSheet.create({
  	container: {
    	flex: 1,
    	backgroundColor: '#fff'
  	},
	appName: {
		fontSize: 0.1 * width,
		color: '#fff',
		textAlign: 'center'
	},
	appNameVGO: {
		fontFamily: 'Raleway-Black'
	},
	appNameM: {
		fontFamily: 'Lobster'
	},
	appNameTrade: {
		fontFamily: 'Raleway-ExtraLightItalic'
	},
	appDescription: {
		fontSize: 0.04 * width,
		color: '#fff',
		textAlign: 'center',
		fontFamily: 'Lato-Light',
		lineHeight: 20
	},
	mainBg: {
		flex: 1,
		padding: 50,
		alignItems: 'center',
    	justifyContent: 'center',
		flexDirection: 'column'
	},
	logo: {
		height: 150,
		resizeMode: 'center'
	},
	bold: {
		fontFamily: 'Lato-Black'
	},
	buttonGradient: {
		borderRadius: 50,
		height: 100,
		width: '80%',
		alignItems: 'center',
		justifyContent: 'center'
	},
	buttonContainer: {
		width: 1 * (width - 100),
		height: 92,
		alignItems: 'center',
		borderRadius: 50
	},
	buttonTextFrame: {
		width: 1 * (width - 108),
		height: '100%',
		backgroundColor: '#230437',
		borderRadius: 50,
	},
	buttonText: {
		textAlign: 'center',
		color: '#ff7949',
		marginTop: 15,
		fontSize: 30,
		fontFamily: 'Raleway-Black'
	},
	buttonTextSmall: {
		textAlign: 'center',
		color: '#fff',
		fontSize: 17,
		fontFamily: 'Raleway-Regular'
	}
});

export default styles;
