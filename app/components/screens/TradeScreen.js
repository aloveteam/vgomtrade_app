import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './TradeScreenCSS';
import { LinearGradient } from 'expo';
import Icon from 'react-native-vector-icons/Feather';
import InventorySlider from '../InventorySlider';
import AcceptTradeModal from '../modals/AcceptTradeModal';

const statisticsGradients = {
  trades: ['#ff9b2c', '#ff3187'],
  skins: ['#138dff', '#cc19ff'],
  users: ['#20ff69', '#ffd21f']
}

class TradeScreen extends Component {

  openSideMenu = () => {
    this.props.navigation.openDrawer();
  }

  render() {
    return (
      <View style={[styles.container]}>
          <LinearGradient style={[styles.mainBg]} colors={['#3e0b5e', '#240735']}>
            <AcceptTradeModal />
            <View style={styles.topNav}>
              <View style={styles.navigationIcon}>
                <TouchableOpacity onPress={this.openSideMenu.bind()}>
                  <Icon name="menu" size={40} color="#fff" />
                </TouchableOpacity>
              </View>
              {
                this.props.fontsLoaded ?
                (
                  <View style={styles.pageTitle}>
                    <Text style={styles.pageTitleText}>TRADE</Text>
                  </View>
                ) : null
              }

              {
                this.props.fontsLoaded ?
                (
                  <View style={styles.userMoney}>
                    <Text style={styles.userMoneyText}>
                      <Image source={require('../../assets/diamond.png')} style={styles.diamondIcon}/>
						{ this.props.authenticatedUser.balance }
                    </Text>
                  </View>
                ) : null
              }
            </View>
            <LinearGradient colors={['rgba(255,255,255,0)', 'rgba(255,255,255,0.1)', 'rgba(255,255,255,0)']} start={{x: 0.0, y: 1.0}} end={{x:1.0, y: 1.0}} style={styles.hr}></LinearGradient>
            {
              this.props.fontsLoaded ?
              (
                <View style={styles.statistics}>
                  <LinearGradient style={[styles.statisticsItem, styles.statisticsItemFirst]} locations={[0, 1]} colors={statisticsGradients.trades} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}>
                    <Text style={[styles.statisticItemCount]}>{ this.props.tradesMade }</Text>
                    <Text style={[styles.statisticItemText]}>TRADES MADE</Text>
                  </LinearGradient>
                  <LinearGradient style={styles.statisticsItem} locations={[0, 1]} colors={statisticsGradients.skins} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}>
                    <Text style={[styles.statisticItemCount]}>{ this.props.skinsOwned }</Text>
                    <Text style={[styles.statisticItemText]}>SKINS OWNED</Text>
                  </LinearGradient>
                  <LinearGradient style={[styles.statisticsItem, styles.statisticsItemLast]} locations={[0, 1]} colors={statisticsGradients.users} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}>
                    <Text style={[styles.statisticItemCount]}>{ this.props.usersOnline }</Text>
                    <Text style={[styles.statisticItemText]}>USERS ONLINE</Text>
                  </LinearGradient>
                </View>
              ) : null
            }
            <LinearGradient colors={['rgba(255,255,255,0)', 'rgba(255,255,255,0.1)', 'rgba(255,255,255,0)']} start={{x: 0.0, y: 1.0}} end={{x:1.0, y: 1.0}} style={styles.hr}></LinearGradient>
            <InventorySlider />
            <View style={styles.swipeFrame}>
              <Text style={styles.swipeText}>Swipe left-right to explore the application.</Text>
            </View>
          </LinearGradient>
        </View>
    )
  }
}

filterEmtpy = (items) => {
  return items.filter(item => !item.empty);
}

function mapStateToProps(state) {
  return {
    fontsLoaded: state.fonts.loaded,
	  authenticatedUser: state.authentication.user,
    skinsOwned: filterEmtpy(state.bot.items).length,
    usersOnline: state.authentication.usersOnline,
    tradesMade: state.authentication.tradesMade
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(TradeScreen);
