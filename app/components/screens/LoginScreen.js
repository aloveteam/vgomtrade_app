import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  WebView,
  Linking
} from 'react-native';
import {
  LinearGradient,
  Font,
  WebBrowser
} from 'expo';
import axios from 'axios';
import LoginModal from '../modals/LoginModal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { openLoginModal } from '../../actions/loginModal';
import styles from './LoginScreenCSS';
import { newNotification, deleteNotification } from '../../actions/notification';
import config from '../../../configs/general';
import DeepLinking from 'react-native-deep-linking';
import shittyQs from 'shitty-qs';
import url from 'url';
import { loginSuccessful } from '../../actions/authentication';

function parseUrlToObject(url) {
  let obj = {};
  url = url.substring(0, url.length - 1);
  let queryParams = url.substring(url.indexOf('?') + 1);
  let query = queryParams.split('&');

  for(let i = 0; i < query.length; i++) {
      let newArray = query[i].split('=');
      obj[newArray[0]] = newArray[1];
  }

  return obj;
}

class LoginScreen extends Component {
    opskinsAuth(url, callback) {
      Linking.addEventListener('url', handleUrl);

      function handleUrl (event) {
        let parametersToLogin = parseUrlToObject(event.url);
        if(parametersToLogin.hasOwnProperty('error')) {
          callback('Opskins login denied!');
        } else {
          callback(null, parametersToLogin);
        }
        Linking.removeEventListener('url', handleUrl);
      }

      Linking.openURL(url);
    }


    openLoginModal = () => {
      axios.get(config.API_SERVER + '/auth/opskins').then((response) => {
        this.opskinsAuth(response.data.url, (err, parametersToLogin) => {
            if (err) {
              this.props.newNotification('danger', err);
              return;
            }
            axios.post(config.API_SERVER + '/auth/opskins/authenticate', parametersToLogin).then((response) => {
              if(response.data.success) {
                this.props.loginSuccessful(response.data);
              } else {
                this.props.newNotification('danger', response.data.message);
              }
            });
        });
      });
    }

    componentWillReceiveProps(nextProps) {
      if(nextProps.authenticated){
        this.props.navigation.navigate('Trade');
      }
    }

  	render() {
    	return (
        <View style={[styles.container]}>
            <LinearGradient style={[styles.mainBg]} colors={['#3e0b5e', '#240735']}>
              <LoginModal />
    					{
    						this.props.fontsLoaded ?
    						(
    							<View style={{ flex: 1 }}>
    								<Text style={[styles.appName]}>
    									<Text style={[styles.appNameVGO]}>VGO </Text>
    									<Text style={[styles.appNameM]}>m</Text>
    									<Text style={[styles.appNameTrade]}>Trade</Text>
    								</Text>
    								<Text style={[styles.appDescription]}>
    									Best Express Trade mobile trading platform.
    								</Text>
    							</View>
    						) : null
    					}
    					<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: '60%'}}>
    						<Image source={require('../../assets/logo.png')} style={styles.logo} />
    					</View>
    					{
    						this.props.fontsLoaded ?
    						(
    							<View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 15, paddingBottom: 15 }}>
    								<Text style={[styles.appDescription]}>Exchange your <Text style={[styles.bold]}>VGO items</Text> with our bots via your smartphone.</Text>
    							</View>
    						) : null
    					}
    					<View style={{ flex: 1, justifyContent: 'flex-end' }}>
    						<LinearGradient
    							style={styles.buttonGradient}
    						 	colors={['#ff9b2c', '#ff3187']}
    							start={{x: 0.0, y: 1.0}} end={{x:1.0, y: 0.0}}
    						>
    							<TouchableOpacity
    								onPress={this.openLoginModal}
    								style={styles.buttonContainer}
    							>
    								{
    									this.props.fontsLoaded ?
    									(
    										<View style={styles.buttonTextFrame}>
    											<Text style={styles.buttonText}>SIGN IN</Text>
    											<Text style={styles.buttonTextSmall}>VIA OPSKINS</Text>
    										</View>
    									) : null
    								}
    							</TouchableOpacity>
    						</LinearGradient>
    					</View>
            </LinearGradient>
          </View>
    	);
  	}
}

function mapStateToProps(state) {
  return {
    authenticated: state.authentication.authenticated,
    fontsLoaded: state.fonts.loaded
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    openLoginModal: openLoginModal,
    newNotification: newNotification,
    loginSuccessful: loginSuccessful
  }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(LoginScreen);
