import {
  StyleSheet,
  Dimensions
} from 'react-native';

let width = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
	},
	mainBg: {
		flex: 1,
		paddingVertical: 30,
    paddingHorizontal: 10,
		alignItems: 'center',
		flexDirection: 'column'
	},
  topNav: {
    flex: 0,
    flexDirection: 'row'
  },
  navigationIcon: {
    flex: 1,
  },
  pageTitle: {
    flex: 1,
    alignItems: 'center'
  },
  userMoney: {
    flex: 1,
    alignItems: 'flex-end'
  },
  pageTitleText: {
    fontFamily: 'Raleway-Regular',
    letterSpacing: 7,
    color: 'rgba(255,255,255,0.7)',
    lineHeight: 40
  },
  userMoneyText: {
    fontFamily: 'Lato-Black',
    color: '#fff',
    lineHeight: 40,
    fontSize: 15
  },
  hr: {
    width: '100%',
    height: 1,
    marginVertical: 15
  },
  statistics: {
    flexDirection: 'row'
  },
  statisticsItem: {
    flex: 1,
    marginHorizontal: 10,
    flexDirection: 'column',
    padding: 5,
    borderRadius: 5
  },
  statisticsItemFirst: {
    marginLeft: 0
  },
  statisticsItemLast: {
    marginRight: 0
  },
  statisticItemCount: {
    fontFamily: 'Lato-Black',
    color: '#fff',
    textAlign: 'center',
    fontSize: 0.045 * width,
    marginBottom: 5
  },
  statisticItemText: {
    fontFamily: 'Lato-Light',
    color: '#fff',
    fontSize: 0.03 * width,
    textAlign: 'center'
  },
  swipeFrame: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#1393ff',
    padding: 5
  },
  swipeText: {
    fontFamily: 'Lato-LightItalic',
    textAlign: 'center',
    color: '#fff'
  }
});

export default styles;
