import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	modal: {
		backgroundColor: '#fff',
		borderRadius: 20,
		marginTop: 50,
		marginBottom: 50
	},
	opskinsIframe: {
		marginTop: 20,
    marginBottom: 20
	},
	closeButton: {
		position: 'absolute',
		bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#3d0661',
    borderBottomLeftRadius: 20,
	  borderBottomRightRadius: 20,
	},
  closeButtonText: {
    textAlign: 'center',
    padding: 20,
    color: '#fff'
  },
  error404: {
    height: 200,
    resizeMode: 'center'
  },
  errorText: {
    textAlign: 'center',
    color: '#240437',
    padding: 20
  }
});

export default styles;
