import React, { Component } from 'react';
import { View, Text, WebView, Image, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closeLoginModal } from '../../actions/loginModal';
import styles from './LoginModalCSS';

class LoginModal extends Component {

  render() {
    return (
      <Modal isVisible={this.props.isLoginModalOpened} backdropOpacity={0.4} style={styles.modal}>
        <View style={{ flex: 1 }}>
        {
          this.props.opskinsURI ?
          (
            <WebView
             source={{uri: this.props.opskinsURI}}
             style={styles.opskinsIframe}
              />
          )
          :
          (
            <View style={{ flex: 1 }}>
              <View style={{ flex: 0}}>
                <Text style={styles.errorText}>Sorry! Login is disabled because opskins.com API is down or you lost connection with our server.</Text>
              </View>
              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginBottom: 60}}>
                <Image source={require('../../assets/404.gif')} style={styles.error404} />
              </View>
            </View>
          )
        }
          <TouchableOpacity
            onPress={this.props.closeLoginModal}
            style={styles.closeButton}
          >
            <Text style={styles.closeButtonText}>Close</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.authentication.error,
    authenticated: state.authentication.authenticated,
    isLoginModalOpened: state.loginModal.opened,
    opskinsURI: state.loginModal.opskinsURI
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    closeLoginModal: closeLoginModal
  }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(LoginModal);
