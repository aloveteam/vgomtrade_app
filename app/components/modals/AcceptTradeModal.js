import React, { Component } from 'react';
import { View, Text, WebView, Image, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closeAcceptModal } from '../../actions/acceptModal';
import styles from './LoginModalCSS';
import { removeTrade, removeTradeError } from '../../actions/trade';
import { fetchItems } from '../../actions/userInventory';
import { fetchBotInventory } from '../../actions/bot';

class AcceptTradeModal extends Component {

  modalClose = () => {
    this.props.fetchItems();
    this.props.fetchBotInventory();
    this.props.removeTrade();
    this.props.removeTradeError();
  }

  render() {
    return (
      <Modal isVisible={this.props.isAcceptModalOpened} backdropOpacity={0.4} style={styles.modal} onModalHide={this.modalClose}>
        <View style={{ flex: 1 }}>
        {
          this.props.trade ?
          (
            <WebView
             source={{uri: 'https://trade.opskins.com/trade-offers/' + this.props.trade.trade_id}}
             style={styles.opskinsIframe}
              />
          )
          :
          (
            <View style={{ flex: 1 }}>
              <View style={{ flex: 0}}>
                <Text style={styles.errorText}>Sorry! Opskins.com API is down or you lost connection with our server.</Text>
              </View>
            </View>
          )
        }
          <TouchableOpacity
            onPress={this.props.closeAcceptModal}
            style={styles.closeButton}
          >
            <Text style={styles.closeButtonText}>Close</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.authentication.error,
    authenticated: state.authentication.authenticated,
    isAcceptModalOpened: state.acceptModal.opened,
    trade: state.trade.trade
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    closeAcceptModal: closeAcceptModal,
    removeTrade: removeTrade,
    removeTradeError: removeTradeError,
    fetchItems: fetchItems,
    fetchBotInventory: fetchBotInventory
  }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(AcceptTradeModal);
