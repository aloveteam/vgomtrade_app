import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image, ScrollView, Picker, TextInput, List, FlatList, Animated, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import InventoryList from './InventoryList';
import styles from './InventorySliderCSS';
import { TabView, SceneMap } from 'react-native-tab-view';
import UserInventory from './tabs/UserInventory';
import BotInventory from './tabs/BotInventory';
import InitiateTrade from './tabs/InitiateTrade';

/* const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(80);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;*/

class InventorySlider extends Component {
  state = {
    index: 0,
    routes: [
      { key: 'user', title: 'Your Inventory' },
      { key: 'our', title: 'Our Inventory' },
      { key: 'trade', title: 'Trade Now' }
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={styles.tabBar}></View>
    );
  };

  _renderScene = SceneMap({
    user: UserInventory,
    our: BotInventory,
    trade: InitiateTrade
  });

  render() {
    return (
        <TabView style={{ width: '100%' }}
          navigationState={this.state}
          renderScene={this._renderScene}
          renderTabBar={this._renderTabBar}
          onIndexChange={this._handleIndexChange}
        />
    );
  }
}

function mapStateToProps(state) {
  return {
    fontsLoaded: state.fonts.loaded,
	  authenticatedUser: state.authentication.user
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(InventorySlider);
