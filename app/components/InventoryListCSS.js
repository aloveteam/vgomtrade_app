import { StyleSheet, Dimensions } from 'react-native';

const numColumns = 3;

const styles = StyleSheet.create({
  loaderContainer: {
	flex: 1,
	justifyContent: 'center',
	alignItems: 'center'
  },
  container: {
    flex: 1
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 3,
    height: (Dimensions.get('window').width - 100) / numColumns, // approximate a square
	borderRadius: 6,
	overflow: 'hidden'
  },
  stroke: {
	alignItems: 'center',
    justifyContent: 'center',
   	width: '100%',
	height: '100%',
	padding: 1,
  borderRadius: 6
  },
  inner: {
	alignItems: 'center',
    justifyContent: 'center',
   	width: '100%',
	height: '100%',
  borderRadius: 6
  },
  image: {
	  height: '90%',
	  width: '90%',
	  resizeMode: 'contain'
  },
  price: {
	  position: 'absolute',
	  right: 5,
	  bottom: 5,
	  paddingHorizontal: 7,
	  paddingVertical: 3,
	  borderRadius: 50
  },
  priceText: {
	fontFamily: 'Lato-Black',
	color: '#fff',
	textShadowColor: 'rgba(0, 0, 0, 0.75)',
  	textShadowOffset: {width: 1, height: 1},
  	textShadowRadius: 2
  },
  skinColor: {
	  position: 'absolute',
	  right: 5,
	  top: 5,
	  width: 10,
	  height: 10,
	  borderRadius: 50
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    color: '#fff',
  },
  hover: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(74,36,97,0.6)',
    borderRadius:6,
    zIndex: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemName: {
    fontFamily: 'Lato-Light',
    color: '#fff',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    	textShadowOffset: {width: 1, height: 1},
    	textShadowRadius: 2,
      textAlign: 'center',
      fontSize: 12
  }
});

export default styles;
